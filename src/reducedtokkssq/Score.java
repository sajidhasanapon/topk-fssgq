/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reducedtokkssq;

/**
 *
 * @author vacuum
 */
public class Score {

    private IntermediateSet vI;
    static double alpha = (double) .33;
    static  double beta = (double) .33;
    static  double gamma = (double) .33;
    private int minConstraint = ReducedTokKSSQ.minimumConstraint;
    private int maxPosMember = ReducedTokKSSQ.maxGroupSize;
    private int minMember = ReducedTokKSSQ.minimumMember;
    private double maxDistance = ReducedTokKSSQ.maxDistance;

    public Score(IntermediateSet vI) {
        this.vI = vI;
    }

    double score() {
//        System.out.println(socialScore());
//        System.out.println(sizeScore());
//        System.out.println(spatialScore());
        return alpha * socialScore() + beta * spatialScore() + gamma * sizeScore();
    }

    double socialScore() {
        int s = vI.getSize();
//        System.out.println(s);
        return (double) vI.getTotalConnectivity() / (s * (s - 1));
    }

    double spatialScore() {
        //System.out.println(vI.getTotalDistance());
        return 1 - (double) vI.getTotalDistance() / (maxDistance * vI.getSize());
    }

    double sizeScore() {
        int s = vI.getSize();
        if (s < minMember) {
            return 0;
        } else {
            return (double) s / maxPosMember;
        }

    }

    double getUpperDistanceForMember() {
        int s = vI.getSize();

//        System.out.println("member size: "+s);
//        System.out.println("maxDistance "+maxDistance);
//        System.out.println("connectivity: "+vI.getTotalConnectivity());
//        System.out.println("distance: "+vI.getTotalDistance());
//        System.out.println((2 * SSTK.minimumConstraint
//                - (double) (2 * vI.getTotalConnectivity()) / (s - 1)));
        //System.out.println("....................................");
        return ((double) (maxDistance * (s + 1)) / beta) * ((double) alpha / (s * (s + 1)) * (2 * ReducedTokKSSQ.minimumConstraint
                - (double) (2 * vI.getTotalConnectivity()) / (s - 1))
                + ((double) gamma / maxPosMember)) + (double) vI.getTotalDistance() / s;
    }

    double getLowerBoundOnConnection(double dMin) {
        int s = vI.getSize();
        //      s=3;
//        System.out.println(" connection lower calculation");
//        System.out.println("member size: " + s);
//        System.out.println("maxDistance " + maxDistance);
//        System.out.println("connectivity: " + vI.getTotalConnectivity());
//        System.out.println("distance: "+vI.getTotalDistance());
        double con = (double) ((s * (s + 1)) / alpha) * (((double) beta / (maxDistance
                * (s + 1))) * (dMin - (double) vI.getTotalDistance() / s) - (double) gamma / maxPosMember)
                + (double) (2 * vI.getTotalConnectivity()) / (s - 1);
//        double con = (double)((s * (s + 1)) / alpha) * (((double)beta/(15 * 
//                (s + 1)))*(14 - (double) 35 / s)-(double) gamma / 5)
//                +(double)(2*4)/(s-1);

//        System.out.println(con);
        return con;
    }

    double getDistanceTermination(int s) {
        if (s > vI.getSize() || s==-1) {
            s = vI.getSize();
        }
        //System.out.println(s);

        //System.out.println("....................................");
        return ((double) (maxDistance * (s + 1)) / beta) * ((double) alpha / (s * (s + 1)) * (2 * s
                - (double) (2 * vI.getTotalConnectivity()) / (s - 1))
                + ((double) gamma / maxPosMember)) + (double) vI.getTotalDistance() / s;

    }

    double advancePruningDistance(int fOld, int nOld, double dOld, int maxDegree, int m) {

        int s = vI.getSize();
        int cNew = 0;

        
        if (maxDegree < m && maxDegree!=-1) {
            if (s < maxDegree) {
                cNew = (maxDegree + s) * (maxDegree - s + 1) + 2 * (m - maxDegree - 1) * maxDegree;

            } else {
                cNew = 2 * (m - s) * maxDegree;
            }
        } else {
//            System.out.println(m+"  "+maxDegree);
            cNew = (m - s) * (m + s - 1);
        }

//        cNew = (m - s) * (m + s - 1);

        double dist = (double) m * ((double) ((double) maxDistance / beta) * ((double) alpha
                * ((double) (vI.getTotalConnectivity() + cNew) / (m * (m - 1))
                - (double) fOld / (nOld * (nOld - 1))) + (double) ((m - nOld) * gamma) / maxPosMember)
                + (double) dOld / nOld) - vI.getTotalDistance();
        return dist;

    }

}
