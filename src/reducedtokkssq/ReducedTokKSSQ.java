/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reducedtokkssq;

import gnu.trove.procedure.TIntProcedure;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsi.Point;
import net.sf.jsi.Rectangle;
import net.sf.jsi.SpatialIndex;
import net.sf.jsi.rtree.RTree;
import java.util.Random;

/**
 *
 * @author vacuum
 */
public class ReducedTokKSSQ {

    /**
     * @param args the command line arguments
     *
     *
     */
    static ArrayList<ResultGroup> resultLists;
    static int groupFound = 0;
    private static int totalCalled = 0;

    static int minimumConstraint;
    static int minimumMember;
    static float maxDistance;
    static int totalMeetingPOint;
    static int c = 0;
    static int rand[];
    static int maxGroupSize;

    static int topK;
    static int queueMem = 11;
    static MemberSet memberSet;
    static MeetingPointSet mPointSet;
    static int presentSampleIndex = -1;

    static int totalTimeIncremantal[];
    static int totalItIncremental[];
    static int totalItApproxIncremental[];
    static int totalTimeApproxIncremantal[];
    static int retrievedMember[];
    static int retrievedMemberApprox[];
    static int retrievedMemberApproxINcremental[];
    static int retrievedMemberIncremental[];
    static MeetingPointSet sampleMeetingPointSet;

    static int totalTime[];
    static int totalIt[];
    static int totalItApprox[];
    static int totalTimeApprox[];

    public static void setDefaultParameter() {

        totalRetrievedMember = 0;
        minimumConstraint = 1;
        minimumMember = 3;
        maxDistance = (float) 15;
        totalMeetingPOint = 2;
        maxGroupSize = 9;
        topK = 1;

        Score.alpha = .33;
        Score.beta = .33;
        Score.gamma = .33;

    }
    static int sampleNum = 1;
    static boolean mpCOllect = false;

    public static void main(String[] args) {
//        MemberSet memberSett = new MemberSet();
//        insertBrightKiteMember(memberSett);
//        insertBrightKiteGraphConnection(memberSett);
//        MeetingPointSet mpss = new MeetingPointSet();
//        insertBrightKiteMeetingPoints(mpss);
        setDefaultParameter();
        ArrayList<Float> samplelist = new ArrayList<>();
        ArrayList<Integer> supportList = new ArrayList<>();

        // minimum member
        samplelist.add((float) 3);
        supportList.add(1);

        if (!mpCOllect) {
//            samplelist.add((float) 5);
//            supportList.add(1);
//            samplelist.add((float) 6);
//            supportList.add(1);
//            samplelist.add((float) 8);
//            supportList.add(1);
//            MIN CONTRRAINT
//            samplelist.add((float) 2);
//            supportList.add(2);
//            samplelist.add((float) 4);
//            supportList.add(2);
//            samplelist.add((float) 5);
//            supportList.add(2);

        }

//        samplelist.add((float) 1);
//        supportList.add(2);
//        samplelist.add((float)3);
//        supportList.add(2);
        //distance
        if (!mpCOllect) {

//            distance
//            samplelist.add((float) .120);
//            supportList.add(3);
//            samplelist.add((float) .160);
//            supportList.add(3);
//
//            samplelist.add((float) .24);
//            supportList.add(3);
//
//            samplelist.add((float) .28);
//            supportList.add(3);
//            samplelist.add((float) .28);
//            supportList.add(3);
//            samplelist.add((float) 2);
//            supportList.add(3);
//            samplelist.add((float) 6);
//            supportList.add(3);
//            samplelist.add((float) 8);
//            supportList.add(3);
//            samplelist.add((float) 2);
//            supportList.add(3);
//            samplelist.add((float) 5);
//            supportList.add(3);
//            samplelist.add((float) .6);
//            supportList.add(3);
//            samplelist.add((float) .7);
//            supportList.add(3);
//            samplelist.add((float) .8);
//            supportList.add(3);
//            samplelist.add((float) .9);
//            supportList.add(3);
//            samplelist.add((float) 1.0);
//            supportList.add(3);
//            samplelist.add((float) .7);
//            supportList.add(3);
//            top k
//            samplelist.add((float) 16);
//            supportList.add(4);
////        samplelist.add((float)16);
////        supportList.add(4);
//            samplelist.add((float) 48);
//            supportList.add(4);
//            samplelist.add((float) 64);
//            supportList.add(4);
//
////        mp 
//            samplelist.add((float) 16);
//            supportList.add(5);
////        samplelist.add((float)16);
////        supportList.add(5);
//            samplelist.add((float) 48);
//            supportList.add(5);
//            samplelist.add((float) 64);
//            supportList.add(5);
//
//            samplelist.add((float) 64);
//            supportList.add(6);
//
//            samplelist.add((float) 64);
//            supportList.add(7);
//
//            samplelist.add((float) 64);
//            supportList.add(8);
        }
        MemberSet memberSet1, memberSet2, memberSet3;
        MeetingPointSet mPointSet, mPointSet1, mPointSet2, mPointSet3;
        memberSet = new MemberSet();
        mPointSet = new MeetingPointSet();
        sampleMeetingPointSet = new MeetingPointSet();

        insertMember(memberSet);
        insertGraphConnection(memberSet);
//        computeNoOfTriangles(edges, memberSet);

        if (!mpCOllect) {
            insertMeetingPoints(mPointSet);

        }
        int feasibleRun[][] = new int[3][samplelist.size()];
        double avgContains50[][] = new double[3][samplelist.size()];
        double avgContains75[][] = new double[3][samplelist.size()];
        double avgContains100[][] = new double[3][samplelist.size()];
        double avgCorrect[][] = new double[3][samplelist.size()];
        int correctMin[][] = new int[3][samplelist.size()];
        int correctMax[][] = new int[3][samplelist.size()];
        int otherRun[][] = new int[3][samplelist.size()];

        totalTime = new int[samplelist.size()];
        totalIt = new int[samplelist.size()];
        totalItApprox = new int[samplelist.size()];
        totalTimeApprox = new int[samplelist.size()];

        totalTimeIncremantal = new int[samplelist.size()];
        totalItIncremental = new int[samplelist.size()];
        totalItApproxIncremental = new int[samplelist.size()];
        totalTimeApproxIncremantal = new int[samplelist.size()];
        retrievedMember = new int[samplelist.size()];
        retrievedMemberApprox = new int[samplelist.size()];
        retrievedMemberApproxINcremental = new int[samplelist.size()];
        retrievedMemberIncremental = new int[samplelist.size()];

        for (int i = 0; i < samplelist.size(); i++) {
            for (int z = 0; z < 3; z++) {
                feasibleRun[z][i] = 0;
                avgContains50[z][i] = 0;
                avgContains75[z][i] = 0;
                avgContains100[z][i] = 0;
                avgCorrect[z][i] = 0;
                correctMin[z][i] = 10000;
                correctMax[z][i] = -10000;
                otherRun[z][i] = 0;
            }

            totalTime[i] = 0;
            totalIt[i] = 0;
            totalItApprox[i] = 0;
            totalTimeApprox[i] = 0;

            totalTimeIncremantal[i] = 0;
            totalItIncremental[i] = 0;
            totalItApproxIncremental[i] = 0;
            totalTimeApproxIncremantal[i] = 0;
            retrievedMember[i] = 0;
            retrievedMemberApprox[i] = 0;
            retrievedMemberApproxINcremental[i] = 0;
            retrievedMemberIncremental[i] = 0;

        }

        for (int j = 1; j <= sampleNum; j++) {
            System.out.println("####################################################################################");
            System.out.println(j + "  ");

            Random rr = new Random();
            if (mpCOllect) {
                float randFactor = rr.nextFloat();
                int meetingDivisionFactor = totalMeetingPOint * 4 + rr.nextInt(totalMeetingPOint * 6);
                System.out.println("randFactor " + randFactor);
                System.out.println("meeting point factor: " + meetingDivisionFactor);
                insertRandomMeetingPoints(mPointSet, meetingDivisionFactor, randFactor);

            }

            int totalMeet = mPointSet.getMeetingPointSetSize();
            rand = new int[totalMeet];

            for (int x = 0; x < totalMeet; x++) {
                rand[x] = x;
            }

//            for (int x = 0; x < totalMeet; x++) {
//                int val = rr.nextInt(totalMeet);
//                int temp = rand[x];
//                rand[x] = rand[val];
//                rand[val] = temp;
//            }
            for (int i = 0; i < samplelist.size(); i++) {

                setDefaultParameter();

                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println(i + "  ");

                presentSampleIndex = i;

                changeParameter(samplelist.get(i), supportList.get(i));

//                maxGroupSize = minimumMember;
                maxGroupSize = 3 * minimumMember;
                maxGroupSize = maxGroupSize / 2;

                ArrayList<Double> topKVal = new ArrayList<>();
                resultLists = new ArrayList<>();
                groupFound = 0;
                System.out.println(minimumMember);
                System.out.println(maxGroupSize);
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                boolean complete = makeResult(memberSet, mPointSet, true, mpCOllect);
                ArrayList<ResultGroup> tempListGroup = new ArrayList<>();

                if (!complete) {
                    resultLists.clear();
                    resultLists = new ArrayList<>();
                    groupFound = 0;

                    if (!mpCOllect) {

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                        makeResultApprox(memberSet, mPointSet, true);
                        System.out.println("jamela");
                    }
                }
                for (ResultGroup rrr : resultLists) {
                    topKVal.add(rrr.score);
                    tempListGroup.add(rrr);
                }
                int topExact = groupFound;

//////
                resultLists.clear();
                resultLists = new ArrayList<>();
                groupFound = 0;

                System.out.println(minimumMember);
                System.out.println(maxGroupSize);
                int scorTh = 0, scorOur = 0;

                if (!mpCOllect && complete) {

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    System.out.println("ekhane");
//                    makeResultApprox(memberSet, mPointSet, true);

                }
                if (topExact == 0 || groupFound == 0) {
//                    continue;
                    System.out.println("ans nai");
                } else {

                    int acc = 0;
                    System.out.println(topKVal.get(0));
                    System.out.println(resultLists.get(0).score);

                    if (topExact == topK && groupFound >= topK / 2) {

                        int xx = topK / 2;

                        feasibleRun[0][i]++;

                        double difRes = 0;
                        int contains50 = 0;
                        int contains75 = 0;
                        int contains100 = 0;
                        int contains = 0;
                        for (int x = 0; x < xx; x++) {
                            difRes += resultLists.get(x).score - topKVal.get(x);
                            if (resultLists.get(x).getGroupSize() == tempListGroup.get(x).getGroupSize()) {
                                if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
                                    acc++;
                                } else {
                                    int cntt = 0;
                                    for (Integer memInteger : resultLists.get(x).getMemberIdList()) {
//                                    System.out.println(memInteger + "  " + tempListGroup.get(x).getMemberIdList().get(cntt));
                                        if (memInteger.equals(tempListGroup.get(x).getMemberIdList().get(cntt))) {
                                            cntt++;
                                        } else {
                                            break;
                                        }

                                    }
                                    if (cntt == resultLists.get(x).getGroupSize()) {
                                        acc++;
                                    } else {
//                                    System.out.println(x);
                                    }

                                }
                            }

                            if (topExact >= topK / 2 && topKVal.get(topK / 2 - 1) <= resultLists.get(x).score) {
                                contains50++;
                            }
                            if (topExact >= (topK * 3) / 4 && topKVal.get((topK * 3) / 4 - 1) <= resultLists.get(x).score) {
                                contains75++;
                            }

                            if (topExact >= topK && topKVal.get(topK - 1) <= resultLists.get(x).score) {
                                contains100++;

                            }

                        }
                        if (acc != xx) {
//                        System.err.println("me le nai");
//                        for (int k = 0; k < totalMeetingPOint; k++) {
//                            System.out.println("mp: " + mPointSet.getList().get(rand[k]).getPosX() + "   " + mPointSet.getList().get(rand[k]).getPosY());
//                        }
                        }

                        if (acc > correctMax[0][i]) {
                            correctMax[0][i] = acc;
                        }
                        if (acc < correctMin[0][i]) {
                            correctMin[0][i] = acc;
                        }

                        avgCorrect[0][i] += (double) acc / xx;
                        avgContains50[0][i] += (double) contains50 / xx;
                        avgContains75[0][i] += (double) contains75 / xx;
                        avgContains100[0][i] += (double) contains100 / xx;

                    } else {

//                        int xx = Math.min(groupFound, topExact);
//                        otherRun[0][i]++;
//
//                        double difRes = 0;
//                        int contains = 0;
//                        for (int x = 0; x < xx; x++) {
//                            difRes += resultLists.get(x).score - topKVal.get(x);
//                            if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
//                                acc++;
//                            }
//                            if (topKVal.get(topExact - 1) <= resultLists.get(x).score) {
//                                contains++;
//                            }
//
//                        }
//
//                        avgCorrect[0][i] += (double) acc / xx;
//                        avgContains50[0][i] += (double) contains / xx;
                    }

                }

                System.out.println("score our: " + scorOur);
                System.out.println("score their:" + scorTh);

                resultLists.clear();
                resultLists = new ArrayList<>();
                groupFound = 0;

                if (!mpCOllect) {

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                    makeResultApprox(memberSet, mPointSet, false);
                }
                if (topExact == 0 || groupFound == 0) {
//                    continue;
                } else {
                    int acc = 0;

                    if (topExact == topK && groupFound >= topK / 2) {

                        int xx = topK / 2;

                        System.out.println(topKVal.get(xx));
                        System.out.println(topKVal.get(topKVal.size() - 1));

                        System.out.println(resultLists.get(0).score);

                        feasibleRun[2][i]++;

                        double difRes = 0;
                        int contains50 = 0;
                        int contains75 = 0;
                        int contains100 = 0;
                        int contains = 0;
                        for (int x = 0; x < xx; x++) {
                            difRes += resultLists.get(x).score - topKVal.get(x);
                            if (resultLists.get(x).getGroupSize() == tempListGroup.get(x).getGroupSize()) {
                                if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
                                    acc++;
                                } else {
                                    int cntt = 0;
                                    for (Integer memInteger : resultLists.get(x).getMemberIdList()) {
//                                    System.out.println(memInteger + "  " + tempListGroup.get(x).getMemberIdList().get(cntt));
                                        if (memInteger.equals(tempListGroup.get(x).getMemberIdList().get(cntt))) {
                                            cntt++;
                                        } else {
                                            break;
                                        }

                                    }
                                    if (cntt == resultLists.get(x).getGroupSize()) {
                                        acc++;
                                    } else {
//                                    System.out.println(x);
                                    }

                                }
                            }

                            if (topExact >= topK / 2 && topKVal.get(topK / 2 - 1) <= resultLists.get(x).score) {
                                contains50++;
                            }
                            if (topExact >= (topK * 3) / 4 && topKVal.get((topK * 3) / 4 - 1) <= resultLists.get(x).score) {
                                contains75++;
                            }

                            if (topExact >= topK && topKVal.get(topK - 1) <= resultLists.get(x).score) {
                                contains100++;

                            }

                        }
                        if (acc != xx) {
//                        System.err.println("me le nai");
//                        for (int k = 0; k < totalMeetingPOint; k++) {
//                            System.out.println("mp: " + mPointSet.getList().get(rand[k]).getPosX() + "   " + mPointSet.getList().get(rand[k]).getPosY());
//                        }
                        }

                        if (acc > correctMax[2][i]) {
                            correctMax[2][i] = acc;
                        }
                        if (acc < correctMin[2][i]) {
                            correctMin[2][i] = acc;
                        }

                        avgCorrect[2][i] += (double) acc / xx;
                        avgContains50[2][i] += (double) contains50 / xx;
                        avgContains75[2][i] += (double) contains75 / xx;
                        avgContains100[2][i] += (double) contains100 / xx;

                    } else {

//                        int xx = Math.min(groupFound, topExact);
//                        otherRun[2][i]++;
//
//                        double difRes = 0;
//                        int contains = 0;
//                        for (int x = 0; x < xx; x++) {
//                            difRes += resultLists.get(x).score - topKVal.get(x);
//                            if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
//                                acc++;
//                            }
//                            if (topKVal.get(topExact - 1) <= resultLists.get(x).score) {
//                                contains++;
//                            }
//
//                        }
//
//                        avgCorrect[2][i] += (double) acc / xx;
//                        avgContains50[2][i] += (double) contains / xx;
                    }

                    resultLists.clear();
                    resultLists = new ArrayList<>();
                    groupFound = 0;

                }

                if (!mpCOllect) {

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                    makeResult(memberSet, mPointSet, false, mpCOllect);
                }
                if (topExact == 0 || groupFound == 0) {
//                    continue;
                } else {
                    int acc = 0;

                    if (topExact == topK && groupFound >= topK / 2) {

                        int xx = topK / 2;

                        System.out.println(topKVal.get(xx));
                        System.out.println(topKVal.get(topKVal.size() - 1));

                        System.out.println(resultLists.get(0).score);

                        feasibleRun[1][i]++;

                        double difRes = 0;
                        int contains50 = 0;
                        int contains75 = 0;
                        int contains100 = 0;
                        int contains = 0;
                        for (int x = 0; x < xx; x++) {
                            difRes += resultLists.get(x).score - topKVal.get(x);
                            if (resultLists.get(x).getGroupSize() == tempListGroup.get(x).getGroupSize()) {
                                if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
                                    acc++;
                                } else {
                                    int cntt = 0;
                                    for (Integer memInteger : resultLists.get(x).getMemberIdList()) {
//                                    System.out.println(memInteger + "  " + tempListGroup.get(x).getMemberIdList().get(cntt));
                                        if (memInteger.equals(tempListGroup.get(x).getMemberIdList().get(cntt))) {
                                            cntt++;
                                        } else {
                                            break;
                                        }

                                    }
                                    if (cntt == resultLists.get(x).getGroupSize()) {
                                        acc++;
                                    } else {
//                                    System.out.println(x);
                                    }

                                }
                            }

                            if (topExact >= topK / 2 && topKVal.get(topK / 2 - 1) <= resultLists.get(x).score) {
                                contains50++;
                            }
                            if (topExact >= (topK * 3) / 4 && topKVal.get((topK * 3) / 4 - 1) <= resultLists.get(x).score) {
                                contains75++;
                            }

                            if (topExact >= topK && topKVal.get(topK - 1) <= resultLists.get(x).score) {
                                contains100++;

                            }

                        }
                        if (acc != xx) {
//                        System.err.println("me le nai");
//                        for (int k = 0; k < totalMeetingPOint; k++) {
//                            System.out.println("mp: " + mPointSet.getList().get(rand[k]).getPosX() + "   " + mPointSet.getList().get(rand[k]).getPosY());
//                        }
                        }

                        if (acc > correctMax[1][i]) {
                            correctMax[1][i] = acc;
                        }
                        if (acc < correctMin[1][i]) {
                            correctMin[1][i] = acc;
                        }

                        avgCorrect[1][i] += (double) acc / xx;
                        avgContains50[1][i] += (double) contains50 / xx;
                        avgContains75[1][i] += (double) contains75 / xx;
                        avgContains100[1][i] += (double) contains100 / xx;

                    } else {

//                        int xx = Math.min(groupFound, topExact);
//                        otherRun[1][i]++;
//
//                        double difRes = 0;
//                        int contains = 0;
//                        for (int x = 0; x < xx; x++) {
//                            difRes += resultLists.get(x).score - topKVal.get(x);
//                            if (Math.abs(resultLists.get(x).score - topKVal.get(x)) < .00000001) {
//                                acc++;
//                            }
//                            if (topKVal.get(topExact - 1) <= resultLists.get(x).score) {
//                                contains++;
//                            }
//
//                        }
//
//                        avgCorrect[1][i] += (double) acc / xx;
//                        avgContains50[1][i] += (double) contains / xx;
                    }
                }

            }

            for (int i = 0; i < samplelist.size(); i++) {

//                minimumConstraint = 3;
//                minimumMember = 6;
//                maxDistance = (float) 1;
//                totalMeetingPOint = 16;
//                maxGroupSize = 9;
//                topK = 16;
//
                setDefaultParameter();
                changeParameter(samplelist.get(i), supportList.get(i));
                maxGroupSize = 3 * minimumMember;
                maxGroupSize = maxGroupSize / 2;

                System.out.println("Minimum size   " + minimumMember);
                System.out.println("Maximum size   " + maxGroupSize);
                System.out.println("min Constraint " + minimumConstraint);
                System.out.println("meeting points " + totalMeetingPOint);
                System.out.println("max distance   " + maxDistance);
                System.out.println("topK           " + topK);

                System.out.println("");
                System.out.println("topk,   topkApprox, topkInc,    topkApprIncr");
                System.out.println("iteration");

                System.out.println(totalIt[i] / j + "," + totalItApprox[i] / j + "," + totalItIncremental[i] / j + "," + totalItApproxIncremental[i] / j + ",");

                System.out.println("time");

                System.out.println(totalTime[i] / j + "," + totalTimeApprox[i] / j + "," + totalTimeIncremantal[i] / j + "," + totalTimeApproxIncremantal[i] / j + ",");

//                System.out.println("#####################   finally   " + totalIt[i] / j + "\t" + totalTime[i] / j);
//                System.out.println("#####################   finally   " + totalItApprox[i] / j + "\t" + totalTimeApprox[i] / j + "\t" + "Approx");
//                System.out.println("#####################   finally   " + totalItIncremental[i] / j + "\t" + totalTimeIncremantal[i] / j);
//                System.out.println("#####################   finally   " + totalItApproxIncremental[i] / j + "\t" + totalTimeApproxIncremantal[i] / j + "\t" + "Approx INcremental");
                System.out.println("total retrieved");

                System.out.println(retrievedMember[i] / j + "," + retrievedMemberApprox[i] / j + "," + retrievedMemberIncremental[i] / j + "," + retrievedMemberApproxINcremental[i] / j);
//                System.out.println(retrievedMember[i] / j + "\t" + retrievedMemberApprox[i] / j + "\t" + retrievedMemberIncremental[i] / j + "\t" + retrievedMemberApproxINcremental[i] / j);
//              
//                System.out.println("total retrieved member                     " + retrievedMember[i] / j);
//                System.out.println("total retrieved member Approx              " + retrievedMemberApprox[i] / j);
//                System.out.println("total retrieved member Incremental         " + retrievedMemberIncremental[i] / j);
//                System.out.println("total retrieved member Approx Incremental  " + retrievedMemberApproxINcremental[i] / j);

//            avgContains50[i] = (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]);
//            avgContains75[i] = (double) avgContains75[i] / feasibleRun[i];
//            avgContains100[i] = (double) avgContains100[i] / feasibleRun[i];
//            avgCorrect[i] = (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]);
                System.out.println("0");
                System.out.println(100.0 * (double) avgCorrect[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains50[0][i] / (feasibleRun[0][i] + otherRun[0][i]) + "," + 100.0 * (double) avgContains75[0][i] / feasibleRun[0][i] + "," + 100.0 * (double) avgContains100[0][i] / feasibleRun[0][i]);
//                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
//
//                // System.out.println("avg contains:   " + avgContains);
//                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
//                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
//                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
                System.out.println("correct max " + correctMax[0][i]);
                System.out.println("correct min " + correctMin[0][i]);

                System.out.println("1");
                System.out.println(100.0 * (double) avgCorrect[1][i] / (feasibleRun[1][i] + otherRun[1][i]) + "," + 100.0 * (double) avgContains50[1][i] / (feasibleRun[1][i] + otherRun[1][i]) + "," + 100.0 * (double) avgContains75[1][i] / feasibleRun[1][i] + "," + 100.0 * (double) avgContains100[1][i] / feasibleRun[1][i]);
//                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
//
//                // System.out.println("avg contains:   " + avgContains);
//                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
//                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
//                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
                System.out.println("correct max " + correctMax[1][i]);
                System.out.println("correct min " + correctMin[1][i]);

                System.out.println("2");
                System.out.println(100.0 * (double) avgCorrect[2][i] / (feasibleRun[2][i] + otherRun[2][i]) + "," + 100.0 * (double) avgContains50[2][i] / (feasibleRun[2][i] + otherRun[2][i]) + "," + 100.0 * (double) avgContains75[2][i] / feasibleRun[2][i] + "," + 100.0 * (double) avgContains100[2][i] / feasibleRun[2][i]);
//                System.out.println("avg correct:  " + (double) avgCorrect[i] / (feasibleRun[i] + otherRun[i]));
//
//                // System.out.println("avg contains:   " + avgContains);
//                System.out.println("avg contains50:   " + (double) avgContains50[i] / (feasibleRun[i] + otherRun[i]));
//                System.out.println("avg contains75:   " + (double) avgContains75[i] / feasibleRun[i]);
//                System.out.println("avg contains100:   " + (double) avgContains100[i] / feasibleRun[i]);
                System.out.println("correct max " + correctMax[2][i]);
                System.out.println("correct min " + correctMin[0][i]);

            }

        }
        if (mpCOllect) {
            saveMeetingPOints();

        }

    }

    public static void saveMeetingPOints() {
        String fileName = "producedMeetingPoint.csv";
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            File file = new File(fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // true = append file
            fw = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fw);

            for (MeetingPoint meetingPoint : sampleMeetingPointSet.getList()) {
                StringBuilder sb = new StringBuilder();

                sb.append(meetingPoint.getPosX());
                sb.append(',');
                sb.append(meetingPoint.getPosY());

                sb.append('\n');

                bw.write(sb.toString());
            }

//                    bw.write(data);
//			System.out.println("Done");
        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }
        System.out.println("mp Collect:" + sampleMeetingPointSet.getList().size());

    }

    private static boolean makeResult(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb, boolean mpCollect) {
        System.out.println("makeResult");
        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));
        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();
        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }

        for (int q = 0; q < it; q++) {
            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());

            if (q == 0) {
                vRest.addMember(0);
                vRest.addMember(4);
                vRest.addMember(1);
                vRest.addMember(3);
                vRest.addMember(2);
                mp.addEntry(0, 1);
                mp.addEntry(4, 1);
                mp.addEntry(1, 2);
                mp.addEntry(3, 14);
                mp.addEntry(2, 15);

            } else {
                vRest.addMember(0);
                vRest.addMember(1);
                vRest.addMember(3);
                vRest.addMember(4);
                vRest.addMember(2);
                mp.addEntry(0, 11);
                mp.addEntry(1, 12);
                mp.addEntry(3, 13);
                mp.addEntry(4, 13);
                mp.addEntry(2, 14);
            }

//            si.nearestN(p, new TIntProcedure() {
//                public boolean execute(int i) {
//                    vRest.addMember(memberSet.getList().get(i).getMemberId());
////                    System.out.println(memberSet.getList().get(i).getMemberId() + "  " + rects.get(i).distance(p));
//                    mp.addEntry(memberSet.getList().get(i).getMemberId(), rects.get(i).distance(p));
//                    return true;
//                }
//
//            }, memberSet.getMemberSetSize(), maxDistance);
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();
//            System.out.print(vRest.getSize()+"        ");
            vRest.pruneUnqualifiedMembers();
//            System.out.println(vRest.getSize());
            tb = System.currentTimeMillis();
            totalMemberConsidered += vRest.getSize();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {
//                as.add(new AssistantClass(vRest, new IntermediateSet(), vRest.getMinimumDistance(0), mPointSet.getList().get(rand[q]),
//                        (double) vRest.getTotalDistance() / vRest.getSize(),
//                        (double) vRest.getTotalDistanceUpTon() / minimumMember, vRest.getMaxDegree(), false));
                RestSet foundRestSet = null;

//        public AssistantClass(RestSet restSet, IntermediateSet intermediateSet, float minDistance, Integer mpId,
//                float avgDistance, float avgDistanceUpToN, int initialMaxDegree, boolean updated, RestSet foundRestSet
//        ) {
                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        vRest.getMaxDegree(), true,
                        foundRestSet));
                if (mpCollect && vRest.getSize() >= minimumMember) {
                    if (!sampleMeetingPointSet.getList().contains(mPointSet.getList().get(rand[q]))) {
                        sampleMeetingPointSet.addMeetingPOint(mPointSet.getList().get(rand[q]));
                    }

                }

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        System.out.println(">>>> " + totalMemberConsidered);
        System.out.println("quque size: " + as.size());
        c = 0;
        totalCalled = 0;
        boolean complete = false;
        if (!mpCollect) {
            complete = generateRankList(as, rects, workingMeetingPointSet, si, comb, start);

        }
        as.clear();
        long now = System.currentTimeMillis();
        System.out.println(".................................... " + totalCalled);
        if (comb) {
            totalIt[presentSampleIndex] += totalCalled;
            totalTime[presentSampleIndex] += tb - ta + now - start;
            retrievedMember[presentSampleIndex] += totalMemberConsidered;

        } else {
            totalItIncremental[presentSampleIndex] += totalCalled;
            totalTimeIncremantal[presentSampleIndex] += tb - ta + now - start;
            retrievedMemberIncremental[presentSampleIndex] += totalMemberConsidered;

        }

        System.out.println(groupFound);

        int ccnt = 0;
        for (ResultGroup r : resultLists) {
            System.out.println("ki");
            ccnt++;
            System.out.println(ccnt + "\n" + r.score);
            r.showMembers();
            System.out.println(r.mpID);

            System.out.println("");
        }
        System.out.println("\n\n");
        as.clear();
        return complete;
    }

    private static void makeResultApprox(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb) {

        System.out.println("makeResult approx");

        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));
        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();
        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }

        for (int q = 0; q < it; q++) {
            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());

            si.nearestN(p, new TIntProcedure() {
                public boolean execute(int i) {
                    vRest.addMember(memberSet.getList().get(i).getMemberId());
//                    System.out.println(memberSet.getList().get(i).getMemberId() + "  " + rects.get(i).distance(p));
                    mp.addEntry(memberSet.getList().get(i).getMemberId(), rects.get(i).distance(p));
                    return true;
                }

            }, memberSet.getMemberSetSize(), maxDistance);

//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();

            vRest.pruneUnqualifiedMembers();
            tb = System.currentTimeMillis();
//            System.out.println(vRest.getMinDegree());
            totalMemberConsidered += vRest.getSize();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {
//                as.add(new AssistantClass(vRest, new IntermediateSet(), vRest.getMinimumDistance(0), mPointSet.getList().get(rand[q]),
//                        (double) vRest.getTotalDistance() / vRest.getSize(),
//                        (double) vRest.getTotalDistanceUpTon() / minimumMember, vRest.getMaxDegree(), false));
                RestSet foundRestSet = null;

//        public AssistantClass(RestSet restSet, IntermediateSet intermediateSet, float minDistance, Integer mpId,
//                float avgDistance, float avgDistanceUpToN, int initialMaxDegree, boolean updated, RestSet foundRestSet
//        ) {
                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        vRest.getMaxDegree(), true,
                        foundRestSet));

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        System.out.println(">>>> " + totalMemberConsidered);
        System.out.println("quque size: " + as.size());
        c = 0;
        totalCalled = 0;

        if (comb) {
            generateRankListApprox(as, rects, workingMeetingPointSet, si, comb);

        } else {
            generateRankListBaseline(as, rects, workingMeetingPointSet, si, comb, start);
        }
//        generateKcore(as, rects, workingMeetingPointSet, si, comb, start);
//        generateKTruss(as, rects, workingMeetingPointSet, si, comb, start);
        long now = System.currentTimeMillis();
        System.out.println(".................................... " + totalCalled);

        as.clear();
        if (comb) {
            retrievedMemberApprox[presentSampleIndex] += totalMemberConsidered;
            totalItApprox[presentSampleIndex] += totalCalled;
            totalTimeApprox[presentSampleIndex] += tb - ta + now - start;

        } else {
            retrievedMemberApproxINcremental[presentSampleIndex] += totalMemberConsidered;
            totalItApproxIncremental[presentSampleIndex] += totalCalled;
            totalTimeApproxIncremantal[presentSampleIndex] += tb - ta + now - start;

        }

//        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
//            ccnt++;
//            System.out.println(ccnt + "  " + r.score);
//            r.showMembers();
//            System.out.println(r.getMinDegree());
//            System.out.println(r.getMp());
//            System.out.println("");
//        }
//        System.out.println("\n\n");
    }

    static int totalRetrievedMember = 0;

    private static void makeResultIncremental(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb) {

        System.out.println("make result incremental");

        totalRetrievedMember = 0;
        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));
        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();

        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }
//        SpatialIndex siList[] = new SpatialIndex[it];

        for (int q = 0; q < it; q++) {

            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());

            IntermediateSet vIntermediateSet = new IntermediateSet();
            RestSet foundRestSet = new RestSet();

            retrieveNextMember(vRest, foundRestSet, mp, memberSet, rects, si);
//          

//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();

            tb = System.currentTimeMillis();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {

                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        -1, true,
                        foundRestSet));

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        totalCalled = 0;
//        totalRetrievedMember = 0;
        generateRankListIncremental(as, rects, workingMeetingPointSet, si, comb);

        as.clear();
        totalItIncremental[presentSampleIndex] += totalCalled;
        retrievedMemberIncremental[presentSampleIndex] += totalRetrievedMember;

        long now = System.currentTimeMillis();
        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
//            ccnt++;
//            System.out.println("\n" + ccnt + "  \n" + r.score);
//            r.showMembers();
////            System.out.println(r.getMp().posX + "  " + r.getMp().posY);
//        }
        totalTimeIncremantal[presentSampleIndex] += tb - ta + now - start;
        System.out.print(c + "\t");
        System.out.println(now - start);
        System.out.println("totalRetrieved   " + totalRetrievedMember);

    }

    private static void makeResultApproxIncremental(MemberSet memberSet, MeetingPointSet mPointSet, boolean comb) {

        System.out.println("make result approx incremental");
        totalRetrievedMember = 0;
        int it = totalMeetingPOint;
        long start = System.currentTimeMillis();
        long ta = 0, tb = 0;
        PriorityQueue<AssistantClass> as = new PriorityQueue<>(queueMem, (a, b) -> a.getminDistance().compareTo(b.getminDistance()));
        int totalMemberConsidered = 0;
        MeetingPointSet workingMeetingPointSet = new MeetingPointSet();
        ArrayList<Rectangle> rects = new ArrayList<>();

        SpatialIndex si = new RTree();
        si.init(null);
        int cnt = 0;
        for (Member m : memberSet.getList()) {
            Rectangle r = new Rectangle(m.getPosX(), m.getPosY(), m.getPosX(), m.getPosY());
            rects.add(r);
            si.add(r, cnt);
            cnt++;
        }
//        SpatialIndex siList[] = new SpatialIndex[it];

        for (int q = 0; q < it; q++) {

            RestSet vRest = new RestSet();
            MeetingPoint mp = new MeetingPoint(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY(), rand[q]);

            final Point p = new Point(mPointSet.getList().get(rand[q]).getPosX(), mPointSet.getList().get(rand[q]).getPosY());

            IntermediateSet vIntermediateSet = new IntermediateSet();
            RestSet foundRestSet = new RestSet();

            retrieveNextMember(vRest, foundRestSet, mp, memberSet, rects, si);
//          

//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
            ta = System.currentTimeMillis();

            tb = System.currentTimeMillis();

            workingMeetingPointSet.addMeetingPOint(mp);

            if (vRest.getSize() != 0) {

                as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
                        -1, true,
                        foundRestSet));

            }
//            si=null;
//            System.out.print(q + "  ");
//            System.out.println(vRest.getSize());
        }

        totalCalled = 0;
//        totalRetrievedMember = 0;
        generateRankListApproxIncremental(as, rects, workingMeetingPointSet, si, comb);

        as.clear();
        retrievedMemberApproxINcremental[presentSampleIndex] += totalRetrievedMember;
        totalItApproxIncremental[presentSampleIndex] += totalCalled;

        long now = System.currentTimeMillis();
        int ccnt = 0;
//        for (ResultGroup r : resultLists) {
//            ccnt++;
//            System.out.println("\n" + ccnt + "  \n" + r.score);
//            r.showMembers();
//            System.out.println(r.getMp());
//        }
        totalTimeApproxIncremantal[presentSampleIndex] += tb - ta + now - start;
        System.out.print(c + "\t");
        System.out.println(now - start);
        System.out.println("totalRetrieved   " + totalRetrievedMember);

    }

    static int adP = 0;

    private static void generateRankListApproxIncremental(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
            SpatialIndex siList, boolean comb) {
        while (queue.size() != 0) {
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize) {
//                System.out.println("??");
                continue;

            }

//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m + "  distance : " + dMin + "           index:    " + index);

                } else {
//                    System.out.println("including new member in restset");
//System.out.println("ekhane ki ase kokhono ?");
                    boolean found = false;
                    if (vRestSet.maxMember < foundRestSet.getSize()) {
                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
                            found = true;

                        }
                    }
                    if (found) {
                        continue;
                    }
//                    System.out.println("agei hoi ni");
//                System.out.println("before");
//                System.out.println(vIntermediateSet.getSize());
//                    System.out.println(vRestSet.getSize());
//                    System.out.println(foundRestSet.getSize());
                    found = retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
//                System.out.println(vRestSet.getSize());
                    if (!found) {
//                    System.out.println("################### no member available");
                        break;
                    }
                    continue;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                    for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println("early termination for member:" + m);
                        break;

                    }

                }

                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

                    double newCon = vIntermediateSet.connectionIfAdded(m);

//                    double rightSide = minimumConstraint - minimumMember + 1 + vIntermediateSet.getSize();
                    double rightSide = (double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0);
//
////                    double rightSide = Math.floor((double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0));
                    if (newCon >= rightSide) {
                        vIntermediateSet.addMember(m, dMin);
//                    System.out.println("before");
//                    vIntermediateSet.showMemberList();
                        boolean updated = false;
                        if (vIntermediateSet.getSize() == minimumMember) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
                                //    resultGroup.getMaxDegree();
//                                System.out.println("famili bad member " + m);
                                notFeas++;
                                continue;
                            }
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.remove(topK - 1);
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                }

                            }

                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                            boolean found = false;
                            if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                    vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                    found = true;

                                }
                            }
                            if (!found) {
//                                System.out.println("notun");
                                retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                            }
                        }

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                        System.out.println(updated);
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
//                            System.out.println(updated);
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, updated,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }

                        break;
                    } else {
//                    System.out.println("bad "+m);
                    }
                } else {

//FOR PREVIOUS GROUP
//                System.out.println("over");
                    int newCon = vIntermediateSet.connectionIfAdded(m);
//                System.out.println(newCon);
//                System.out.println(minimumConstraint);
//                    System.out.println("hmmmmmmmmmmmmmmmmmmmmmmmm");
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

                    if (newCon >= minimumConstraint) {

                        //NO CHECKING FOR TERMINATION......
                        if (groupFound < topK) {
//                        System.out.println("\nmember kom\n");
                            vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            resultLists.add(resultGroup);
//                            System.out.println("added");
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();
                            vRestSet.popMember(index);
                            totalCalled++;
                            if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                boolean found = false;
                                if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                    for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                        vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                        found = true;

                                    }
                                }
                                if (!found) {
//                                System.out.println("notun");
                                    retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                }
                            }

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, true,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }

                            break;
//                            continue;
                        }

                        if (assistantClass.updated) {
//                        System.out.println("\n baap included hoise\n");
                            vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            ResultGroup r = resultLists.get(topK - 1);
                            boolean updated = false;
                            if (r.score < resultGroup.score) {
//                                System.out.println("added");
                                resultLists.remove(topK - 1);
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                            } else {
//                                System.out.println("not added");
                            }
                            vRestSet.popMember(index);
                            totalCalled++;

                            if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                boolean found = false;
                                if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                    for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                        vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                        found = true;

                                    }
                                }
                                if (!found) {
//                                System.out.println("notun");
                                    retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                }
                            }

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, updated,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }

                            break;
                        } else {

//                            System.out.println("\n baap included hoini **************\n");
                            double terminatingDistance = s.getDistanceTermination(assistantClass.initialMaxDegree);
//                        System.out.println("terminating distance: " + terminatingDistance);

                            if (dMin > terminatingDistance) {
//                                System.out.println("termination due to distance upper bound");
//                            adP++;

                                break;
                            }
//                        System.out.println("upper distance for member: " + s.getUpperDistanceForMember());
                            if (dMin < s.getUpperDistanceForMember()) {
//                                System.out.println("distance lemma");

                                vIntermediateSet.addMember(m, dMin);
//                                System.out.println("before");
//                                vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    resultLists.remove(topK - 1);
                                    updated = true;
                                    resultLists.add(resultGroup);
//                                    System.out.println("added");
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
                                } else {
//                                    System.out.println("not added");
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                    boolean found = false;
                                    if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                            found = true;

                                        }
                                    }
                                    if (!found) {
//                                System.out.println("notun");
                                        retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                    }
                                }

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, false,
                                                foundRestSet));

                                    }

                                }

                                break;
//                                continue;
                            }
                            double conLower = s.getLowerBoundOnConnection(dMin);

                            if ((float) 2 * newCon >= conLower) {
//                                System.out.println("connection Lemma");
//                            totalCalled++;

                                vIntermediateSet.addMember(m, dMin);
//                                System.out.println("before");
//                                vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    resultLists.remove(topK - 1);
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                    System.out.println("added");
                                    //                  resultGroup.getMaxDegree();
                                } else {
//                                    System.out.println("not added");
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                    boolean found = false;
                                    if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                            found = true;

                                        }
                                    }
                                    if (!found) {
//                                System.out.println("notun");
                                        retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                    }
                                }

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, false,
                                                foundRestSet));

                                    }

                                }

                                break;
                            } else {

//                                System.out.println("ekhane");
                            }
                        }

                    } else {
//                        System.out.println("familily bad member " + m);

                    }

                }
            }
            assistantClass = null;
        }

    }

    private static void generateRankListIncremental(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
            SpatialIndex siList, boolean comb) {

        while (queue.size() != 0) {
//            System.out.println(queue.size());
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
            if (vIntermediateSet.getSize() >= maxGroupSize) {
//                System.out.println("??");
                continue;

            }
//            System.out.println("meeting point " + mp.id);
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            while (vIntermediateSet.getSize() < maxGroupSize) {
//                System.out.println("hm");

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                    System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                    System.out.println("including new member in restset");
//System.out.println("ekhane ki ase kokhono ?");
                    boolean found = false;
                    if (vRestSet.maxMember < foundRestSet.getSize()) {
                        for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                            vRestSet.addMember(foundRestSet.getMemberList().get(q));
//                              vRest.addMember(memberSet.getList().get(i).getMemberId());
//                
                            found = true;

                        }
                    }
                    if (found) {
                        continue;
                    }
//                    System.out.println("agei hoi ni");
//                System.out.println("before");
//                System.out.println(vIntermediateSet.getSize());
//                    System.out.println(vRestSet.getSize());
//                    System.out.println(foundRestSet.getSize());
                    found = retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
//                System.out.println(vRestSet.getSize());
                    if (!found) {
//                    System.out.println("################### no member available");
                        break;
                    }
                    continue;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    for (int i = minimumMember; i <= maxGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), -1,
                                i);
//                         double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
//                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
//                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println("$$$$$$$$$$$$$$$$$early termination for member:" + m.getMemberId());
                        break;

                    }

                }

                if (vIntermediateSet.getSize() < minimumMember) {

//                double newCon=vIntermediateSet.connectionIfAdded(m);
                    double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
//                double rightSide = (double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0);

//                double rightSide = Math.floor((double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0));
                    double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());
                    if (newCon <= rightSide) {
//                        System.out.println("dgukse");

                        vIntermediateSet.addMember(m, dMin);
                        boolean updated = false;
                        if (vIntermediateSet.getSize() == minimumMember) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("group formed but not satisfied group:");
//                            //    resultGroup.getMaxDegree();
//                                System.out.println("feasible not found for member: " + m.getMemberId());
                                notFeas++;
                                continue;
                            }
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.remove(topK - 1);
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                }

                            }

                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                            boolean found = false;
                            if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                    vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                    found = true;

                                }
                            }
                            if (!found) {
//                                System.out.println("notun");
                                retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                            }
                        }

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }

//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

//                    System.out.println("\n bad for familiarity constraint\n ");
//                    System.out.println(newCon);
//                    System.out.println(minimumConstraint);
//                    System.out.println(minimumMember - 1);
//                    System.out.println(rightSide);
                        adP++;
                    }
                } else {

//                System.out.println("yeap");
                    int newCon = vIntermediateSet.connectionIfAdded(m);

                    if (newCon >= minimumConstraint) {
//                    System.out.println("no");
                        if (groupFound < topK) {
                            vIntermediateSet.addMember(m, dMin);

//                        System.out.println("group sob ase ni");
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();

                            vRestSet.popMember(index);
                            totalCalled++;

                            if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                                boolean found = false;
                                if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                    for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                        vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                        found = true;

                                    }
                                }
                                if (!found) {
//                                System.out.println("notun");
                                    retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                                }
                            }

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }

//                            System.out.println("after");
//                            vIntermediateSet.showMemberList();
                            break;
                        }

                        vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        ResultGroup r = resultLists.get(topK - 1);
                        boolean updated = false;
                        if (r.score < resultGroup.score) {
                            resultLists.remove(topK - 1);
                            updated = true;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        if (vRestSet.getSize() == 0) {
//                            System.out.println("including new member in restset");
                            boolean found = false;
                            if (vRestSet.maxMember < foundRestSet.getSize()) {
//                                System.out.println("ekhane");
                                for (int q = vRestSet.maxMember; q < foundRestSet.getSize(); q++) {
                                    vRestSet.addMember(foundRestSet.getMemberList().get(q));
                                    found = true;

                                }
                            }
                            if (!found) {
//                                System.out.println("notun");
                                retrieveNextMember(vRestSet, foundRestSet, mpSet.getList().get(mpID), memberSet, rects, siList);
                            }
                        }

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

                    }

                }
            }

//            assistantClass = null;
        }

    }

    private static boolean retrieveNextMember(RestSet vRest, RestSet foundRestSet, MeetingPoint mp, MemberSet memberSet, ArrayList<Rectangle> rects,
            SpatialIndex siFast
    ) {
        final Point p = new Point(mp.getPosX(), mp.getPosY());

        int l = vRest.getSize();
//        int cnt = 0;
        ArrayList<Integer> memberId = new ArrayList<>();
        ArrayList<Integer> iContainliList = new ArrayList<>();
        siFast.nearestN(p, new TIntProcedure() {
            public boolean execute(int i) {
                //                  System.out.println("Rectangle " + i + " " + rects.get(i) + ", distance=" + rects.get(i).distance(p));
//                    if (vRest.getSize() > l) {
//                        return false;
//                    }
//                    siFast.delete(rects.get(i), i);

//                    System.out.println("retrievnig:::    " + memberSet.getList().get(i).getMemberId());
                memberId.add(memberSet.getList().get(i).getMemberId());
                iContainliList.add(i);
//                if (cnt > totalRetrievedMember) {
//                    foundRestSet.addMember(memberSet.getList().get(i).getMemberId());
//                    vRest.addMember(memberSet.getList().get(i).getMemberId());
//
//                    mp.addEntry(memberSet.getList().get(i).getMemberId(), rects.get(i).distance(p));
//                    
//
//                }
//                    System.out.println("called");
                return true;
            }

        }, foundRestSet.getSize() + 1, (float) maxDistance);
//        System.out.println(memberId.size() + "   " + foundRestSet.getSize());
        if (memberId.size() > foundRestSet.getSize()) {
            while (memberId.size() > foundRestSet.getSize()) {
                vRest.addMember(memberId.get(foundRestSet.getSize()));

                mp.addEntry(memberId.get(foundRestSet.getSize()), rects.get(iContainliList.get(foundRestSet.getSize())).distance(p));
                foundRestSet.addMember(memberId.get(foundRestSet.getSize()));

                totalRetrievedMember++;

            }

//            vRest.addMember(memberId.get(foundRestSet.getSize()));
//
//            mp.addEntry(memberId.get(foundRestSet.getSize()), rects.get(iContainliList.get(foundRestSet.getSize())).distance(p));
//            foundRestSet.addMember(memberId.get(foundRestSet.getSize()));
//
//            totalRetrievedMember++;
//            System.out.println(totalRetrievedMember);
            return true;
        }

//        if (vRest.getSize() > l) {
////            System.out.println("ota: " + totalRetrievedMember);
//            totalRetrievedMember += vRest.getSize() - l;
//            return true;
//        }
//        else {
//            if (groupFound < topK) {
////                System.out.println("kahini hoise");
//                maxDistance =maxDistance+maxDistance;
//                if(retrieveNextMember(vRest, foundRestSet, p, memberSet, rects, siFast)){
//                    return true;
//                }
//            }
//            return false;
//
//        }
        return false;

    }

    private static boolean generateRankList(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
            SpatialIndex siList, boolean comb, long start) {

        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > 1000000) {
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
            System.out.println("\\\\");
            System.out.print(mpID-(mpID-1)*2);
            System.out.print(" & ");
            
            vIntermediateSet.showMemberList();
            System.out.print(" & ");
            
            vRestSet.showMemberList();
            System.out.print(" & ");
            
//            System.out.println("");
//            System.out.println("dist "+ assistantClass.minDistance);
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize) {
//                System.out.println("??");
System.out.println("y");
                continue;

            }
//         

//            System.out.println("before");
//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
System.out.println("hm");
                    break;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                        for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

                        System.out.println(" a.t & ");
                        break;

                    }

                }

                if (vIntermediateSet.getSize() < minimumMember) {
                    System.out.print(" $ " + (char) (m + 'a') + " $ &");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);

                    double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
                    double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());
                    if (newCon <= rightSide) {

                        vIntermediateSet.addMember(m, dMin);
                        boolean updated = false;

                        if (vIntermediateSet.getSize() == minimumMember) {

//                             public ResultGroup(ArrayList<Integer> imemberList,
//             float totalDistance,
//            int groupSize, int totalConnectivity, double score, Integer mpId) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("group formed but not satisfied group:");
//                            //    resultGroup.getMaxDegree();
                                System.out.print ("n.f &");
                                notFeas++;
                                continue;
                            }
 System.out.print(" included &");
                       
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                                System.out.print(" and recorded &");
//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.remove(topK - 1);
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                    System.out.print(" and recorded &");
                                }

                            }

                        }else{
                             System.out.print(" included  &");
                       
                        }
                        vRestSet.popMember(index);
                        totalCalled++;
                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();

                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

//                    System.out.println("\n bad for familiarity constraint\n ");
                    System.out.print("f.prun &");
//                    System.out.println(minimumConstraint);
//                    System.out.println(minimumMember - 1);
//                    System.out.println(rightSide);
                        adP++;
                    }
                } else {

//                System.out.println("yeap");
                    System.out.print(" $ " + (char) (m + 'a') + " $ &");
//                    System.out.print(m + " con ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
                    int newCon = vIntermediateSet.connectionIfAdded(m);

                    if (newCon >= minimumConstraint) {
//                    System.out.println("no");
                        if (groupFound < topK) {
                            vIntermediateSet.addMember(m, dMin);

//                        System.out.println("group sob ase ni");
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;

                            System.out.println("included &");
                            System.out.print(" and recorded  &");
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();

                            vRestSet.popMember(index);
                            totalCalled++;
                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, false,
                                            foundRestSet));

                                }

                            }
//                            System.out.println("after");
//                            vIntermediateSet.showMemberList();
                            break;

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                            continue;
                        }

                        vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        ResultGroup r = resultLists.get(topK - 1);
                        boolean updated = false;

                        System.out.println("included &");
                        if (r.score < resultGroup.score) {
                            resultLists.remove(topK - 1);
                            updated = true;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                            System.out.print(" and recorded &");
//                            resultGroup.getMaxDegree();
                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();

                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());
System.out.print("f.prun & ");
                    }

                }
            }
//            assistantClass.restSet.getMemberList().clear();
//            assistantClass.intermediateSet.getDistanceList().clear();
//            assistantClass.intermediateSet.getMemberList().clear();
//            assistantClass.restSet.getDistanceList().clear();
        }
        return true;
    }

    private static boolean generateRankListBaseline(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
            SpatialIndex siList, boolean comb, long start) {
        System.out.println("baseline");
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > 1000000) {
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize) {
//                System.out.println("??");
                continue;

            }
//         

//            System.out.println("before");
//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
                    break;
                }

//                if (resultLists.size() == topK) {
//                    ResultGroup r = resultLists.get(topK - 1);
//                    int ttlGroupSize = vIntermediateSet.getGroupSize()
//                            + vRestSet.getUnvisitedMemberCount() + 1;
//                    int sz = vIntermediateSet.getSize();
//                    boolean adTerminate = true;
//                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//                    for (int i = minimumMember; i <= maxGroupSize; i++) {
//
////                        for (int i = minimumMember; i <= ttlGroupSize; i++) {
//                        if (sz >= i) {
//                            continue;
//                        }
//                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
//                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
//                                i);
////                        System.out.println(pruningDistance);
////                        System.out.println(dMin * (i - vIntermediateSet.getSize()));
//
//                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
////                        System.out.println("\n\n\n\ntermination advance");
////                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
////                        //   System.out.println(m.getMemberId());
////                        System.out.println(pruningDistance);
////                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));
//
//                            adTerminate = false;
//                            break;
//                        }
//                    }
//                    if (adTerminate) {
////                    adP++;
//
////                        System.out.println("early termination for member:" + m.getMemberId());
//                        break;
//
//                    }
//
//                }
                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.print(m + " con ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);

//                    double newCon = minimumConstraint - vIntermediateSet.connectionIfAdded(m);
//                    double rightSide = (minimumMember - 1 - vIntermediateSet.getSize());
//                    if (newCon <= rightSide) {
                    vIntermediateSet.addMember(m, dMin);
                    boolean updated = false;
                    if (vIntermediateSet.getSize() == minimumMember) {

//                             public ResultGroup(ArrayList<Integer> imemberList,
//             float totalDistance,
//            int groupSize, int totalConnectivity, double score, Integer mpId) {
                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        int minCon = resultGroup.getMinDegree();
                        if (minCon < minimumConstraint) {
                            vIntermediateSet.popMember(m, dMin);

//                            System.out.println("group formed but not satisfied group:");
//                            //    resultGroup.getMaxDegree();
//                                System.out.println("feasible not found for member: " + m.getMemberId());
                            notFeas++;
                            continue;
                        }
                        if (groupFound < topK) {
                            groupFound++;
                            updated = true;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                        } else {
                            ResultGroup r = resultLists.get(topK - 1);
                            if (r.score < resultGroup.score) {
                                updated = true;
                                resultLists.remove(topK - 1);
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                            }

                        }

                    }
                    vRestSet.popMember(index);
                    totalCalled++;
                    RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                            vRestSet.getVisited(), vRestSet.maxMember);
                    newRestSet.markAllUnVisited();

                    IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                            vIntermediateSet.getMaxFriend(),
                            vIntermediateSet.getTotalDistance(),
                            vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                    if (newRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

//                    if (comb) {
                    vIntermediateSet.popMember(m, dMin);

                    if (vRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

//                    }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                    break;

//                    } else {
////                        System.out.println("familily bad member " + m.getMemberId());
//
////                    System.out.println("\n bad for familiarity constraint\n ");
////                    System.out.println(newCon);
////                    System.out.println(minimumConstraint);
////                    System.out.println(minimumMember - 1);
////                    System.out.println(rightSide);
//                        adP++;
//                    }
                } else {

//                System.out.println("yeap");
//                    System.out.print(m + " con ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
                    int newCon = vIntermediateSet.connectionIfAdded(m);

                    if (newCon >= minimumConstraint) {
//                    System.out.println("no");
                        if (groupFound < topK) {
                            vIntermediateSet.addMember(m, dMin);

//                        System.out.println("group sob ase ni");
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();

                            vRestSet.popMember(index);
                            totalCalled++;
                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

//                            if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, false,
                                        foundRestSet));

                            }

//                            }
//                            System.out.println("after");
//                            vIntermediateSet.showMemberList();
                            break;

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                            continue;
                        }

                        vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                        ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getSize(),
                                vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                        ResultGroup r = resultLists.get(topK - 1);
                        boolean updated = false;
                        if (r.score < resultGroup.score) {
                            resultLists.remove(topK - 1);
                            updated = true;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                        }
                        vRestSet.popMember(index);
                        totalCalled++;
                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

//                        if (comb) {
                        vIntermediateSet.popMember(m, dMin);

                        if (vRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, false,
                                    foundRestSet));

                        }

//                        }
//                        System.out.println("after");
//                        vIntermediateSet.showMemberList();
                        break;

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

                    }

                }
            }
//            assistantClass.restSet.getMemberList().clear();
//            assistantClass.intermediateSet.getDistanceList().clear();
//            assistantClass.intermediateSet.getMemberList().clear();
//            assistantClass.restSet.getDistanceList().clear();
        }
        return true;
    }

    private static boolean generateKcore(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
            SpatialIndex siList, boolean comb, long start) {
        System.out.println(queue.size());
        System.out.println("problem");
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > 1000000) {
                System.out.println("hm");
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
            if (vIntermediateSet.getSize() >= maxGroupSize) {
                continue;

            }
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index = 0;
            int size = vRestSet.getSize() + vIntermediateSet.getSize();
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
                } else {
                    break;
                }
//                System.out.println(m);
                vIntermediateSet.addMember(m, dMin);

                boolean updated = false;
                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                int minCon = resultGroup.getMinDegree();

                if (vIntermediateSet.getSize() == minimumMember && minCon < minimumConstraint) {

                    vIntermediateSet.popMember(m, dMin);
                    continue;
                }

                if (minCon >= minimumConstraint) {
//                    vIntermediateSet.popMember(m, dMin);
                    if (groupFound < topK) {
                        groupFound++;
                        updated = true;
                        resultLists.add(resultGroup);
                        Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                    } else {
                        ResultGroup r = resultLists.get(topK - 1);
                        if (r.score < resultGroup.score) {
                            updated = true;
                            resultLists.remove(topK - 1);
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                        }

                    }

                }

                vRestSet.popMember(index);
                totalCalled++;
                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                        vRestSet.getVisited(), vRestSet.maxMember);
                newRestSet.markAllUnVisited();

                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getMaxFriend(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getTotalConnectivity());

                if (newRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                if (comb) {
                    vIntermediateSet.popMember(m, dMin);

                    if (vRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

                }

                break;

            }

        }

        return true;
    }

    private static boolean generateKTruss(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
            SpatialIndex siList, boolean comb, long start) {
        System.out.println(queue.size());
        while (queue.size() != 0) {
//            System.out.println(queue.size());
            long now = System.currentTimeMillis();
            if (now - start > 1000000) {
                System.out.println("hm");
                return false;
            }
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;
            if (vIntermediateSet.getSize() >= maxGroupSize) {
                continue;

            }
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index = 0;
            int size = vRestSet.getSize() + vIntermediateSet.getSize();
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
                } else {
                    break;
                }
//                System.out.println(m);
                vIntermediateSet.addMember(m, dMin);
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//        
//                ArrayList<Integer> firstMember = new ArrayList<>();
//                ArrayList<Integer> secondMember = new ArrayList<>();
//                ArrayList<Integer> noOfTriangles = new ArrayList<>();
//                preprocessKTruss(firstMember, secondMember, noOfTriangles, vIntermediateSet);
//                while (true) {
//                    computeNoOfTriangles(firstMember, secondMember, noOfTriangles, vIntermediateSet);
//                    int totalEdges = firstMember.size();
//                    ArrayList<Integer> disIntegers = new ArrayList<>();
//                    for (int i = 0; i < totalEdges; i++) {
//                        if (noOfTriangles.get(i) < minimumConstraint) {
//                            disIntegers.add(firstMember.get(i));
//                            disIntegers.add(secondMember.get(i));
//                            firstMember.remove(i);
//                            secondMember.remove(i);
//                            noOfTriangles.remove(i);
//                            totalEdges--;
//                            i--;
//                            
//                        }
//                    }
//                    if (disIntegers.isEmpty()) {
//                        break;
//                    }
//                    for (Integer dInteger : disIntegers) {
//                        totalEdges = firstMember.size();
//                        for (int i = 0; i < totalEdges; i++) {
//                            if (firstMember.get(i) == dInteger || secondMember.get(i) == dInteger) {
//                                firstMember.remove(i);
//                                secondMember.remove(i);
//                                noOfTriangles.remove(i);
//                                 totalEdges--;
//                                i--;
//                               
//                            }
//                        }
//                    }
//
//                }
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                boolean updated = false;
                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getSize(),
                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                int minCon = resultGroup.getMinDegree();

                if (minCon >= minimumConstraint) {
//                    vIntermediateSet.popMember(m, dMin);
                    if (groupFound < topK) {
                        groupFound++;
                        updated = true;
                        resultLists.add(resultGroup);
                        Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                    } else {
                        ResultGroup r = resultLists.get(topK - 1);
                        if (r.score < resultGroup.score) {
                            updated = true;
                            resultLists.remove(topK - 1);
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                        }

                    }

                }

                vRestSet.popMember(index);
                totalCalled++;
                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                        vRestSet.getVisited(), vRestSet.maxMember);
                newRestSet.markAllUnVisited();

                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                        vIntermediateSet.getMaxFriend(),
                        vIntermediateSet.getTotalDistance(),
                        vIntermediateSet.getTotalConnectivity());

                if (newRestSet.getSize() != 0) {
                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                            assistantClass.initialMaxDegree, false,
                            foundRestSet));

                }

                if (comb) {
                    vIntermediateSet.popMember(m, dMin);

                    if (vRestSet.getSize() != 0) {
                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                assistantClass.initialMaxDegree, false,
                                foundRestSet));

                    }

                }

                break;

            }

        }

        return true;
    }

    static int notFeas = 0;

    private static void generateRankListApprox(PriorityQueue<AssistantClass> queue, ArrayList<Rectangle> rects, MeetingPointSet mpSet,
            SpatialIndex siList, boolean comb) {
        while (queue.size() != 0) {
            AssistantClass assistantClass = queue.remove();
            Integer mpID = assistantClass.meetingPointId;
            IntermediateSet vIntermediateSet = assistantClass.intermediateSet;
            RestSet vRestSet = assistantClass.restSet;
            RestSet foundRestSet = assistantClass.foundRestSet;

            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
//            if (vIntermediateSet.getSize() + vRestSet.getSize() < minimumMember || vRestSet.getSize() == 0) {
//                continue;
//            }
            if (vIntermediateSet.getSize() >= maxGroupSize) {
//                System.out.println("??");
                continue;

            }

//            System.out.println(queue.size());
            Score s = new Score(vIntermediateSet);
            boolean resultUpdated = false;

            Integer m = null;
            float dMin = 100;
            int index;
//            vIntermediateSet.showMemberList();
//            vRestSet.showMemberList();

            int size = vRestSet.getSize() + vIntermediateSet.getSize();
//        System.out.println(size);
//        System.out.println(vRestSet.isEmpty());
            //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//            while (size >= minimumMember && !vRestSet.isEmpty()) {
            while (vIntermediateSet.getSize() < maxGroupSize && !vRestSet.isEmpty()) {

                if (vRestSet.foundUnvisitedVertex()) {
                    m = vRestSet.topMember();
                    index = vRestSet.getLocalMemberIndex(m);
                    vRestSet.markVisited(index);
//                    System.out.println(mpID);

                    dMin = mpSet.getList().get(mpID).getMemberDistance(m);
//                System.out.println("member id: " + m.getMemberId() + "  distance : " + dMin + "           index:    " + index);

                } else {
//                System.out.println("ar member nai ");
                    break;
                }

                if (resultLists.size() == topK) {
                    ResultGroup r = resultLists.get(topK - 1);
                    int ttlGroupSize = vIntermediateSet.getGroupSize()
                            + vRestSet.getUnvisitedMemberCount() + 1;
                    int sz = vIntermediateSet.getSize();
                    boolean adTerminate = true;
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    for (int i = minimumMember; i <= maxGroupSize; i++) {

//                    for (int i = minimumMember; i <= ttlGroupSize; i++) {
                        if (sz >= i) {
                            continue;
                        }
                        double pruningDistance = s.advancePruningDistance(r.getTotalConnectivity(),
                                r.getGroupSize(), r.getTotalDistance(), assistantClass.initialMaxDegree,
                                i);
//                        System.out.println(pruningDistance);
//                        System.out.println(dMin * (i - vIntermediateSet.getSize()));

                        if (pruningDistance > (double) dMin * (i - vIntermediateSet.getSize())) {
//                        System.out.println("\n\n\n\ntermination advance");
//                        System.out.println((ttlGroupSize - vIntermediateSet.getSize()));
//                        //   System.out.println(m.getMemberId());
//                        System.out.println(pruningDistance);
//                        System.out.println((double) dMin * (ttlGroupSize - vIntermediateSet.getSize()));

                            adTerminate = false;
                            break;
                        }
                    }
                    if (adTerminate) {
//                    adP++;

//                        System.out.println("early termination for member:" + m.getMemberId());
                        break;

                    }

                }

                if (vIntermediateSet.getSize() < minimumMember) {
//                    System.out.println("ki");
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

                    double newCon = vIntermediateSet.connectionIfAdded(m);

//                    double rightSide = minimumConstraint - minimumMember + 1 + vIntermediateSet.getSize();
                    double rightSide = (double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0);
//
////                    double rightSide = Math.floor((double) (minimumConstraint * vIntermediateSet.getSize()) / (minimumMember - 1.0));
                    if (newCon >= rightSide) {
                        vIntermediateSet.addMember(m, dMin);
//                    System.out.println("before");
//                    vIntermediateSet.showMemberList();
                        boolean updated = false;
                        if (vIntermediateSet.getSize() == minimumMember) {
                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            int minCon = resultGroup.getMinDegree();
                            if (minCon < minimumConstraint) {
                                vIntermediateSet.popMember(m, dMin);

//                            System.out.println("\n\n\n\n\n ekhane\n\n\n\n");
                                //    resultGroup.getMaxDegree();
//                            System.out.println("famili bad member "+m.getMemberId());
                                notFeas++;
                                continue;
                            }
                            if (groupFound < topK) {
                                groupFound++;
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();

//                            resultGroup.showConnection();
                            } else {
                                ResultGroup r = resultLists.get(topK - 1);
                                if (r.score < resultGroup.score) {
                                    updated = true;
                                    resultLists.remove(topK - 1);
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
//                                resultGroup.showConnection();
                                }

                            }

                        }
                        vRestSet.popMember(index);
                        totalCalled++;

                        RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                vRestSet.getVisited(), vRestSet.maxMember);
                        newRestSet.markAllUnVisited();

                        IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                vIntermediateSet.getMaxFriend(),
                                vIntermediateSet.getTotalDistance(),
                                vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                        if (newRestSet.getSize() != 0) {
                            queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                    assistantClass.initialMaxDegree, updated,
                                    foundRestSet));

                        }

                        if (comb) {
                            vIntermediateSet.popMember(m, dMin);

                            if (vRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, assistantClass.updated,
                                        foundRestSet));

                            }

                        }

                        break;
                    } else {
//                    System.out.println("bad");
                    }
                } else {
//                    System.out.print(m + " con " + dMin + "  ");
//                    vIntermediateSet.showMemberList();
//                    System.out.println(assistantClass.updated);
//                    System.out.println(mpID);

//                    System.out.println("hmmmmmmmmmmmmmmmmmmmmmmmm");
//                    System.out.print(m+" con ");
//                    vIntermediateSet.showMemberList();
//FOR PREVIOUS GROUP
//                System.out.println("over");
                    int newCon = vIntermediateSet.connectionIfAdded(m);
//                System.out.println(newCon);
//                System.out.println(minimumConstraint);

                    if (newCon >= minimumConstraint) {

                        //NO CHECKING FOR TERMINATION......
                        if (groupFound < topK) {
//                        System.out.println("\nmember kom\n");
                            vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);

                            groupFound++;
                            resultLists.add(resultGroup);
                            Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                        resultGroup.getMaxDegree();
                            vRestSet.popMember(index);
                            totalCalled++;

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, true,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, assistantClass.updated,
                                            foundRestSet));

                                }

                            }

                            break;
//                            continue;
                        }

                        if (assistantClass.updated) {
//                        System.out.println("\n baap included hoise\n");
                            vIntermediateSet.addMember(m, dMin);
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();

                            ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getSize(),
                                    vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                            ResultGroup r = resultLists.get(topK - 1);
                            boolean updated = false;
                            if (r.score < resultGroup.score) {
                                resultLists.remove(topK - 1);
                                updated = true;
                                resultLists.add(resultGroup);
                                Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                            resultGroup.getMaxDegree();
                            }
                            vRestSet.popMember(index);
                            totalCalled++;

                            RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                    vRestSet.getVisited(), vRestSet.maxMember);
                            newRestSet.markAllUnVisited();

                            IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                    vIntermediateSet.getMaxFriend(),
                                    vIntermediateSet.getTotalDistance(),
                                    vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                            if (newRestSet.getSize() != 0) {
                                queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                        assistantClass.initialMaxDegree, updated,
                                        foundRestSet));

                            }

                            if (comb) {
                                vIntermediateSet.popMember(m, dMin);

                                if (vRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, assistantClass.updated,
                                            foundRestSet));

                                }

                            }

                            break;
                        } else {

//                        System.out.println("\n baap included hoini **************\n");
                            double terminatingDistance = s.getDistanceTermination(assistantClass.initialMaxDegree);
//                        System.out.println("terminating distance: " + terminatingDistance);

                            if (dMin > terminatingDistance) {
//                            System.out.println("termination due to distance upper bound");
//                            adP++;

                                break;
                            }
//                        System.out.println("upper distance for member: " + s.getUpperDistanceForMember());
                            if (dMin < s.getUpperDistanceForMember()) {
//                            System.out.println("distance lemma");

                                vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    resultLists.remove(topK - 1);
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
//                                resultGroup.getMaxDegree();
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, assistantClass.updated,
                                                foundRestSet));

                                    }

                                }

                                break;
//                                continue;
                            }
                            double conLower = s.getLowerBoundOnConnection(dMin);

                            if ((float) 2 * newCon >= conLower) {
//                            System.out.println("connection Lemma");
//                            totalCalled++;

                                vIntermediateSet.addMember(m, dMin);
//                            System.out.println("before");
//                            vIntermediateSet.showMemberList();

                                ResultGroup resultGroup = new ResultGroup(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getSize(),
                                        vIntermediateSet.getTotalConnectivity(), s.score(), mpSet.getList().get(mpID).id);
                                ResultGroup r = resultLists.get(topK - 1);
                                boolean updated = false;
                                if (r.score < resultGroup.score) {
                                    resultLists.remove(topK - 1);
                                    updated = true;
                                    resultLists.add(resultGroup);
                                    Collections.sort(resultLists, (a, b) -> b.getIntegerScore().compareTo(a.getIntegerScore()));
                                    //                  resultGroup.getMaxDegree();
                                }
                                vRestSet.popMember(index);
                                totalCalled++;

                                RestSet newRestSet = new RestSet(vRestSet.getMemberList(),
                                        vRestSet.getVisited(), vRestSet.maxMember);
                                newRestSet.markAllUnVisited();

                                IntermediateSet newIntermediateSet = new IntermediateSet(vIntermediateSet.getMemberList(),
                                        vIntermediateSet.getMaxFriend(),
                                        vIntermediateSet.getTotalDistance(),
                                        vIntermediateSet.getTotalConnectivity());
//                        System.out.println("before");
//                        vIntermediateSet.showMemberList();
//                    System.out.println("rest");
//                    vRestSet.showMemberList();

//   as.add(new AssistantClass(vRest, new IntermediateSet(), mp.getMemberDistance(vRest.getMemberId(0)), q,
//                        vRest.getMaxDegree(), false,
//                        foundRestSet));
                                if (newRestSet.getSize() != 0) {
                                    queue.add(new AssistantClass(newRestSet, newIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                            assistantClass.initialMaxDegree, updated,
                                            foundRestSet));

                                }

                                if (comb) {
                                    vIntermediateSet.popMember(m, dMin);

                                    if (vRestSet.getSize() != 0) {
                                        queue.add(new AssistantClass(vRestSet, vIntermediateSet, mpSet.getList().get(mpID).getMemberDistance(vRestSet.getMemberId(0)), mpID,
                                                assistantClass.initialMaxDegree, assistantClass.updated,
                                                foundRestSet));

                                    }

                                }

                                break;
                            } else {
//                                System.out.println("ekhane");
                            }
                        }

                    } else {
//                        System.out.println("familily bad member " + m.getMemberId());

                    }

                }
            }
        }

    }

    static void insertMeetingPoints(MeetingPointSet mps) {
//        String csvFile = "meetingPoints.csv";
//        String csvFile = "modifiedMeetingPOints.csv";
//        String csvFile = "GowallaMeetingPoint.csv";
//        String csvFile = "checkMeet.csv";
        String csvFile = "producedMeetingPoint.csv";

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
//                System.out.println(line);
                String[] strings = line.split(cvsSplitBy);

                mps.addMeetingPOint(new MeetingPoint(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]), cnt));

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        mps.showMeetingPoints();
    }

    private static void insertMember(MemberSet memberSet) {
        String csvFile = "member - Copy.csv";
//        String csvFile = "Brightkite_location.csv";
//        String csvFile = "Gowalla_location.csv";
//
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] strings = line.split(cvsSplitBy);
                memberSet.addMember(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]), cnt);
                cnt++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        memberSet.showMembers();
//        System.out.println(memberSet.getMemberSetSize());
    }

    private static void insertGraphConnection(MemberSet memberSet) {
        String csvFile = "connection - Copy.csv";
//        String csvFile = "brightKiteCOnnection.csv";
//        String csvFile = "GowallaCOnnection.csv";

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] strings = line.split(cvsSplitBy);
                for (String s : strings) {
                    int cnt2 = Integer.parseInt(s) - 1;
                    memberSet.addConnection(cnt, cnt2);
//                    edges.add(new Edge(cnt, cnt2));
                }
                cnt++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        memberSet.showGraphConnection();
    }

    public static <T> T mostCommon(List<T> list) {
        Map<T, Integer> map = new HashMap<>();

        for (T t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Entry<T, Integer> max = null;

        for (Entry<T, Integer> e : map.entrySet()) {
            if (max == null || e.getValue() > max.getValue()) {
                max = e;
            }
        }

        return max.getKey();

    }

    private static void changeParameter(float get, int fuck) {
        switch (fuck) {
            case 1:
                minimumMember = (int) get;
                System.out.println("changing min member to: " + get);
                break;
            case 2:
                minimumConstraint = (int) get;
                System.out.println("changing min constraint to: " + get);
                break;
            case 3:
                maxDistance = get;
                System.out.println("changing max distance to: " + get);
                break;
            case 4:
                topK = (int) get;
                System.out.println("changing min top k to: " + get);
                break;
            case 5:
                totalMeetingPOint = (int) get;
                System.out.println("changing mp total to: " + get);
                break;

            case 6:
//                totalMeetingPOint = (int) get;
                Score.alpha = .5;
                Score.beta = .25;
                Score.gamma = .25;
                System.out.println("changing alpha to: ");
                break;
            case 7:
//                totalMeetingPOint = (int) get;
                Score.alpha = .25;
                Score.beta = .5;
                Score.gamma = .25;

                System.out.println("changing beta to: ");
                break;
            case 8:

                Score.alpha = .25;
                Score.beta = .25;
                Score.gamma = .5;

//                totalMeetingPOint = (int) get;
                System.out.println("changing gamma to: ");
                break;

        }
    }

    private static void computeNoOfTriangles(ArrayList<Integer> firstMember, ArrayList<Integer> secondMember, ArrayList<Integer> noOfTringles, IntermediateSet vIntermediateSet) {
        int l = firstMember.size();
        for (int i = 0; i < l; i++) {
            Member m1 = memberSet.getList().get(firstMember.get(i));
            Member m2 = memberSet.getList().get(secondMember.get(i));
            for (int friend : m1.getFriendList()) {
                if (vIntermediateSet.getMemberList().contains(friend) && m2.getFriendList().contains(friend)) {
                    noOfTringles.set(i, noOfTringles.get(i) + 1);
                }
            }

            for (int friend : m1.getFriendList()) {
                System.out.print(" " + friend);
            }
            System.out.println("");

            for (int friend : m2.getFriendList()) {
                System.out.print(" " + friend);
            }
            System.out.println("");
            System.out.println(firstMember.get(i) + "  " + secondMember.get(i) + "  " + noOfTringles.get(i));

        }
    }

    private static void preprocessKTruss(ArrayList<Integer> firstMember, ArrayList<Integer> secondMember, ArrayList<Integer> noOfTringles, IntermediateSet vIntermediateSet) {
        System.out.println("hm");
        vIntermediateSet.showMemberList();

        for (int member : vIntermediateSet.getMemberList()) {
            System.out.println("member: " + member);
            for (int friend : memberSet.getList().get(member).getFriendList()) {
                System.out.print(" " + friend);
                if (firstMember.contains(friend) && secondMember.contains(member)) {
                    continue;
                }
                if (vIntermediateSet.getMemberList().contains(friend)) {
                    firstMember.add(member);
                    secondMember.add(friend);
                    noOfTringles.add(0);
                }
            }
            System.out.println("");
        }

        for (int i = 0; i < firstMember.size(); i++) {
            System.out.println(firstMember.get(i) + "  " + secondMember.get(i));
        }

    }

    private static class Location {

        float longitude;
        float latitude;

        public Location(float longitude, float latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public void show() {
            System.out.println(longitude + "  " + latitude);
        }

        public float getLongitude() {
            return longitude;
        }

        public float getLatitude() {
            return latitude;
        }

    }

    static ArrayList<Integer> vertexNoLocation = new ArrayList<>();

    private static void insertBrightKiteMember(MemberSet memberSet) {

        List<Location> list = new ArrayList<>();

        String csvFile = "Gowalla_totalCheckins.txt";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "\\s+";
        int cnt = 0;
        boolean newMember = false;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] strings = line.split(cvsSplitBy);
//                memberSet.addMember(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]), cnt);
//                if (cnt > 2) {
//                    break;
//                }
                int l = Integer.parseInt(strings[0]);
                if (l > cnt) {
//                        mostCommon(list).show();
                    Location location = mostCommon(list);

                    list.clear();

                    for (int i = 0; i < l - cnt; i++) {
                        memberSet.addMember(location.getLongitude(), location.getLatitude(), cnt + i);

                    }
                    cnt = l;

                }

                try {
                    if (strings.length >= 3) {
                        list.add(new Location(Float.parseFloat(strings[2]), Float.parseFloat(strings[3])));

                    } else {
                        System.out.println(cnt);
                        for (String s : strings) {
                            System.out.println(s);
                        }
                        System.out.println("");
                    }

                } catch (Exception e) {
                    for (String s : strings) {
                        System.err.println(s);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Location location = mostCommon(list);

        memberSet.addMember(location.getLongitude(), location.getLatitude(), cnt);

        System.out.println(vertexNoLocation.size());
        System.out.println(memberSet.getMemberSetSize());

        PrintWriter pw;
        try {
            pw = new PrintWriter(new File("Gowalla_location.csv"));
            for (Member m : memberSet.getList()) {
                StringBuilder sb = new StringBuilder();

                sb.append(m.getPosX());
                sb.append(',');
                sb.append(m.getPosY());
                sb.append(',');
                sb.append(m.getMemberId());

                sb.append('\n');

                pw.write(sb.toString());

            }
            pw.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReducedTokKSSQ.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        try {
            pw = new PrintWriter(new File("vertexNoLocation.csv"));
            for (Integer i : vertexNoLocation) {
                StringBuilder sb = new StringBuilder();

                sb.append(i);
                sb.append('\n');

                pw.write(sb.toString());

            }
            pw.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReducedTokKSSQ.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("done!");
    }

    private static void insertBrightKiteGraphConnection(MemberSet memberSet) {
        String csvFile = "Gowalla_edges.txt";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "\\s+";
        int cnt = 0;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                String[] strings = line.split(cvsSplitBy);
                if (Integer.parseInt(strings[0]) < memberSet.getMemberSetSize() && Integer.parseInt(strings[1]) < memberSet.getMemberSetSize()) {
                    memberSet.addConnection(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]));
                    cnt++;

                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        PrintWriter pw;
//        try {
//            pw = new PrintWriter(new File("E:/_thesis/Data/Brightkite/Brightkite_location.csv"));
//            for (Member m : memberSet.getList()) {
//                StringBuilder sb = new StringBuilder();
//
//                sb.append(m.getPosX());
//                sb.append(',');
//                sb.append(m.getPosY());
//                sb.append(',');
//                sb.append(m.getMemberId());
//
//                sb.append('\n');
//
//                pw.write(sb.toString());
//
//            }
//            pw.close();
//
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(SSTKQueueModified.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
        try {
            pw = new PrintWriter(new File("GowallaCOnnection.csv"));
            for (Member m : memberSet.getList()) {
                StringBuilder sb = new StringBuilder();

                int l = m.getFriendList().size();
                for (int i = 0; i < l; i++) {
                    sb.append(m.getFriendList().get(i));
                    if (i == l - 1) {
                        break;
                    }
                    sb.append(',');
                }

                sb.append("\n");
                pw.write(sb.toString());

            }

            pw.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReducedTokKSSQ.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    static void insertRandomMeetingPoints(MeetingPointSet mps, int div, float randFactor) {

        float maxLongitude;
        float minLongitude;
        float maxLatitude;
        float minLatitude;
//        gowalla

//        div = div / 7;
//        maxLatitude = 55;
//        minLatitude = 15;
//        maxLongitude = -50;
//        minLongitude = -120;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//
////        maxLatitude = 75;
////        minLatitude = 35;
////        maxLongitude = 30;
////        minLongitude = -20;
////
////        for (int i = 0; i < div; i++) {
////            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
////            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//////            System.out.println("\n");
//////            System.out.println(posX);
//////            System.out.println(posY);
////            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
////
////        }
////      problem ase eitate
//        maxLatitude = 20;
//        minLatitude = -8;
//        maxLongitude = 110;
//        minLongitude = 90;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//
////        not so good 
//        maxLatitude = 50;
//        minLatitude = 20;
//        maxLongitude = 130;
//        minLongitude = 90;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//
//        maxLatitude = -15;
//        minLatitude = -45;
//        maxLongitude = 155;
//        minLongitude = 135;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//
////eita valo......................
//        maxLatitude = -10;
//        minLatitude = -40;
//        maxLongitude = -40;
//        minLongitude = -80;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//
//        maxLatitude = 25;
//        minLatitude = 2;
//        maxLongitude = -60;
//        minLongitude = -120;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//
////      kharap
//        maxLatitude = 30;
//        minLatitude = 10;
//        maxLongitude = 60;
//        minLongitude = 35;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//
//        }
//        
//        
////  brighitkite
        div = div / 5;

        maxLongitude = -50;
        minLongitude = -160;
        maxLatitude = 60;
        minLatitude = 5;
        for (int i = 0; i < div; i++) {
            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

        }

        maxLatitude = -15;
        minLatitude = -55;
        maxLongitude = 180;
        minLongitude = 90;
        for (int i = 0; i < div; i++) {
            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

        }
        maxLatitude = 50;
        minLatitude = -15;
        maxLongitude = 150;
        minLongitude = 60;

        for (int i = 0; i < div; i++) {
            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

        }
        maxLatitude = 40;
        minLatitude = -5;
        maxLongitude = 80;
        minLongitude = -20;

        for (int i = 0; i < div; i++) {
            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

        }
        maxLongitude = 60;
        minLongitude = -10;
        maxLatitude = 80;
        minLatitude = 5;
        for (int i = 0; i < div; i++) {
            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
//            System.out.println("\n");
//            System.out.println(posX);
//            System.out.println(posY);
            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));

        }
//normal
//        maxLongitude = 10;
//        minLongitude = 0;
//        maxLatitude = 10;
//        minLatitude = 0;
//
//        for (int i = 0; i < div; i++) {
//            float posX = minLatitude + i * ((float) (maxLatitude - minLatitude) / div);
//            float posY = minLongitude + i * ((float) (maxLongitude - minLongitude) / div);
////            System.out.println("\n");
////            System.out.println(posX);
////            System.out.println(posY);
//            mps.addMeetingPOint(new MeetingPoint(posX, posY, i));
//        }
    }

    private static class AssistantClass {

        RestSet restSet;
        IntermediateSet intermediateSet;
        float minDistance;
        Integer meetingPointId;
        int initialMaxDegree;
        boolean updated;
        RestSet foundRestSet;

        public AssistantClass(RestSet restSet, IntermediateSet intermediateSet, float minDistance, Integer mpId,
                int initialMaxDegree, boolean updated, RestSet foundRestSet
        ) {
            this.restSet = restSet;
            this.intermediateSet = intermediateSet;
            this.minDistance = minDistance;
            this.meetingPointId = mpId;
            this.initialMaxDegree = initialMaxDegree;
            this.updated = updated;
            this.foundRestSet = foundRestSet;
        }

        Integer getminDistance() {
            return (int) (minDistance * 10000000);
        }

        Integer getSize() {
            return (int) restSet.getSize();
        }

    }

}
