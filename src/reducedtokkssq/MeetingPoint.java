/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reducedtokkssq;

import java.util.ArrayList;

/**
 *
 * @author vacuum
 */
public class MeetingPoint {
    
    float posX;
    float posY;
    int id;
    ArrayList<Float> distanceList;
    ArrayList<Integer> memberIdList;

    public MeetingPoint(float posX, float posY, int id) {
        this.posX = posX;
        this.posY = posY;
        this.id = id;
        distanceList = new ArrayList<>();
        memberIdList = new ArrayList<>();
    }
    
    public float getPosX() {
        return posX;
    }
    
    public float getPosY() {
        return posY;
    }
    
    public void addEntry(Integer memberId, float distance) {
        memberIdList.add(memberId);
        distanceList.add(distance);
    }
    
    public float getMemberDistance(Integer memberId) {
        int i=memberIdList.indexOf(memberId);
        return distanceList.get(i);
    }
    
}
